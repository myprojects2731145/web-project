const express = require('express');
const path = require('path');
const app = express();
const port = 5000;

// Set views directory
app.set('views', path.join(__dirname, 'views'));

// Set view engine to use HTML files
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// Set up static file serving from the 'public' directory
//app.use(express.static('public'));
app.use(express.static(path.join(__dirname, 'public')));
// Define routes
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'index.html'));
});
app.get('/home', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'index.html'));
  });
app.get('/about', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'about.html'));
});

app.get('/portofolio', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'portofolio.html'));
});

// Handle 404 errors
app.use((req, res) => {
  res.status(404).send('<h1>404: Halaman tidak ditemukan 😭😭😭</h1>');
});
// Start the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

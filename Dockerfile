FROM node:20.8.0
COPY . /app
RUN cd /app
RUN apt-get update -y --allow-unauthenticated
EXPOSE 5000
CMD ["node", "/app/app.js"]
